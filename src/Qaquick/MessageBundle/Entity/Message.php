<?php

namespace Qaquick\MessageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Qaquick\UserBundle\Entity\User;

/**
 * Message
 */
class Message
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $text;

    /**
     * @var \DateTime
     */
    private $criticalDate;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var integer
     */
    private $hidden;

    /**
     * @var integer
     */
    private $deleted;

    /**
     * @var Collection
     */
    private $replies;

    /**
     * @var Message
     */
    private $parent;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Collection
     */
    private $destinationUsers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->creationDate = new \DateTime();
        $this->hidden = 0;
        $this->deleted = 0;
        $this->replies = new ArrayCollection();
        $this->destinationUsers = new ArrayCollection();
    }

    /**
     * To string method
     *
     * @return string
     */
    public function __toString()
    {
        return (string)$this->title;
    }

    /**
     * To array method
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'id' => $this->id,
            'title' => $this->title,
            'text' => $this->text,
            'sender' => $this->user->__toString(),
        );
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Message
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Message
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set criticalDate
     *
     * @param \DateTime $criticalDate
     * @return Message
     */
    public function setCriticalDate($criticalDate)
    {
        $this->criticalDate = $criticalDate;

        return $this;
    }

    /**
     * Get criticalDate
     *
     * @return \DateTime 
     */
    public function getCriticalDate()
    {
        return $this->criticalDate;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Message
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set hidden
     *
     * @param integer $hidden
     * @return Message
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * Get hidden
     *
     * @return integer 
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     * @return Message
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Add replies
     *
     * @param Message $replies
     * @return Message
     */
    public function addReply(Message $replies)
    {
        $this->replies[] = $replies;

        return $this;
    }

    /**
     * Remove replies
     *
     * @param Message $replies
     */
    public function removeReply(Message $replies)
    {
        $this->replies->removeElement($replies);
    }

    /**
     * Get replies
     *
     * @return Collection 
     */
    public function getReplies()
    {
        return $this->replies;
    }

    /**
     * Set parent
     *
     * @param Message $parent
     * @return Message
     */
    public function setParent(Message $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return Message 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Message
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add destinationUsers
     *
     * @param User $destinationUsers
     * @return Message
     */
    public function addDestinationUser(User $destinationUsers)
    {
        $this->destinationUsers[] = $destinationUsers;

        return $this;
    }

    /**
     * Remove destinationUsers
     *
     * @param User $destinationUsers
     */
    public function removeDestinationUser(User $destinationUsers)
    {
        $this->destinationUsers->removeElement($destinationUsers);
    }

    /**
     * Get destinationUsers
     *
     * @return Collection 
     */
    public function getDestinationUsers()
    {
        return $this->destinationUsers;
    }
}
