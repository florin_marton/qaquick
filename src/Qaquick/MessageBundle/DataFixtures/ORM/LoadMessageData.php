<?php
namespace Qaquick\MessageBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture,
    Doctrine\Common\DataFixtures\OrderedFixtureInterface,
    Doctrine\Common\Persistence\ObjectManager,
    Symfony\Component\DependencyInjection\ContainerAwareInterface,
    Symfony\Component\DependencyInjection\ContainerInterface,
    Qaquick\UserBundle\Repository\UserRepository,
    Qaquick\UserBundle\Entity\User,
    Qaquick\MessageBundle\Entity\Message;

class LoadMessageData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $em)
    {
        /** @var UserRepository $userRepo */
        $userRepo = $em->getRepository('UserBundle:User');
        $users = $userRepo->findAll();

        $title = 'Hello everyone from ';
        $text = 'Welcome message text from ';

        /** @var User $user */
        foreach ($users as $user) {
            $message = new Message();
            $message->setUser($user);
            $message->setTitle($title . $user->getFirstName());
            $message->setText($text . $user->__toString());
            $message->setCriticalDate(new \DateTime('+ 1 day'));

            /** @var User $destinationUser */
            foreach ($users as $destinationUser) {
                if ($destinationUser->getId() !== $user->getId())
                $message->addDestinationUser($destinationUser);
                $destinationUser->addAddressedMessage($message);
            }

            $em->persist($message);
            $em->flush();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }
}