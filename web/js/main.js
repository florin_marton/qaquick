window.App = {
    Models: {},
    Views: {},
    Collections: {}
};

window.template = function(id) {
    return _.template($('#' + id).html());
};


$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

App.Models.Message = Backbone.Model.extend({
    urlRoot: '/message'
});

App.Collections.Messages = Backbone.Collection.extend({
    url: '/messages',
    model: App.Models.Message
});

App.Collections.SentMessages = Backbone.Collection.extend({
    url: '/sent-messages',
    model: App.Models.Message
});

App.Views.Messages = Backbone.View.extend({
    el: '.page',
    template: template('messages_list_template'),
    render: function(messages) {
        var that = this;
        messages.fetch({
           success: function(messages) {
               that.$el.html(that.template({messages: messages.models}));
           }
        });
    }
});

App.Views.Message = Backbone.View.extend({
    el: '.page',

    template: template('message_template'),

    render: function(message) {
        this.$el.html(this.template({message: message}));
    },

    events: {
        'submit .message_reply_form': 'reply',
        'click .edit_message_trigger': 'edit_message_mode',
        'click .save_message_changes': 'save_message_changes',
        'click .edit_reply_trigger': 'edit_reply_mode',
        'click .save_reply_changes': 'save_reply_changes'
    },

    reply: function(ev) {
        ev.preventDefault();
        var form = $(ev.currentTarget);
        var formDetails = form.serializeObject();
        var reply = new App.Models.Message();
//        ToDo: validation
        reply.save(formDetails, {
            success: function(reply) {
                router.navigate('#/view/' + reply.get('id'), {trigger: true})
            }
        });

        return false;
    },

    edit_message_mode: function(ev) {
        var editButton = $(ev.currentTarget);
        var messageContainer = editButton.parents('.message');
        messageContainer.find('.save_message_changes').show();
        editButton.hide();

        var titleContainer = messageContainer.find('.message_title');
        var textContainer = messageContainer.find('.message_text');

        var titleInput = $('.hidden_controls .input_title')
            .clone()
            .addClass('mainMessageInput');

        var textInput = $('.hidden_controls .input_text')
            .clone()
            .addClass('mainMessageInput');

        titleInput.val(titleContainer.html());
        textInput.val(textContainer.html());

        titleContainer.hide().parent().append(titleInput);
        textContainer.hide().parent().prepend(textInput);
    },

    save_message_changes: function(ev) {
        var saveButton = $(ev.currentTarget);
        var messageContainer = saveButton.parents('.message');
        messageContainer.find('.edit_message_trigger').show();
        saveButton.hide();

        var titleContainer = messageContainer.find('.message_title');
        var textContainer = messageContainer.find('.message_text');

        var titleInput = messageContainer.find('.input_title');
        var textInput = messageContainer.find('.input_text');

        titleContainer.html(titleInput.val()).show();
        textContainer.html(textInput.val()).show();

        var data = messageContainer.find('.mainMessageInput').serializeObject();
        var message = new App.Models.Message();
        //ToDo: validation
        message.save(data, {
            success: function(message) {
                //ToDo: handle errors
                console.log(message);
            }
        });

        titleInput.remove();
        textInput.remove();
    },

    edit_reply_mode: function(ev) {
        var editButton = $(ev.currentTarget);
        var replyContainer = editButton.parents('.reply');
        replyContainer.find('.save_reply_changes').show();
        editButton.hide();

        var textContainer = replyContainer.find('.reply_text');

        var textInput = $('.hidden_controls .input_text')
            .clone()
            .addClass('replyInput');

        textInput.val(textContainer.html().trim());

        textContainer.hide().parent().append(textInput);
    },

    save_reply_changes: function(ev) {
        var saveButton = $(ev.currentTarget);
        var replyContainer = saveButton.parents('.reply');
        replyContainer.find('.edit_reply_trigger').show();
        saveButton.hide();

        var textContainer = replyContainer.find('.reply_text');

        var textInput = replyContainer.find('.input_text');

        textContainer.html(textInput.val()).show();

        var data = replyContainer.find('.replyInput').serializeObject();
        var message = new App.Models.Message();
        //ToDo: validation
        message.save(data, {
            success: function(message) {
                //ToDo: handle errors
                console.log(message);
            }
        });

        textInput.remove();
    }
});

App.Views.MessageForm = Backbone.View.extend({
    el: '.page',
    template: template('message_form_template'),
    render: function() {
        this.$el.html(this.template());
        this.$el.find('.form_datetime').datetimepicker({
            format: 'yyyy-mm-dd hh:ii'
        });
        this.$el.find('#inputUsers').select2();
    },
    events: {
        'submit .message_form': 'saveMessage'
    },
    saveMessage: function(ev) {
        ev.preventDefault();
        var form = $(ev.currentTarget);
        var formDetails = form.serializeObject();
        var message = new App.Models.Message();
//        ToDo: validation
        message.save(formDetails, {
            success: function(message) {
                router.navigate('', {trigger: true})
            }
        });

        return false;
    }
});

App.Router = Backbone.Router.extend({
    routes: {
        '': 'list',
        'sent': 'sent_list',
        'view/:id': 'view_message',
        'write': 'write_message'
    }
});
var router = new App.Router();
var messagesList = new App.Views.Messages();
var messagesView = new App.Views.Message();

router.on('route:list', function() {
    var messages = new App.Collections.Messages();
    messagesList.render(messages);
});

router.on('route:sent_list', function() {
    var messages = new App.Collections.SentMessages();
    messagesList.render(messages);
});

router.on('route:view_message', function(id) {
    var message = new App.Models.Message();
    message.set({id: id});
    message.fetch({
        success: function(result) {
            if (typeof(result['error']) != 'undefined') {
                alert(result['error']);
            } else {
                messagesView.render(result);
            }
        }
    });
});

router.on('route:write_message', function() {
    var messageFormView = new App.Views.MessageForm();
    messageFormView.render();
});

Backbone.history.start();

var InfiniteAjaxRequest = function (uri) {
    $.ajax({
        url: uri,
        success: function(data) {
            // do something with "data"
            if (data.length > 0) {
                console.log(data);
            }
            InfiniteAjaxRequest (uri);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError);
        }
    });
};

//$(window).load(function(){
//    InfiniteAjaxRequest('/check-events');
//});