<?php

namespace Qaquick\MessageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Qaquick\UserBundle\Entity\User;
use Qaquick\MessageBundle\Entity\Message;

class AjaxController extends Controller
{
    public function indexAction()
    {
        return new Response();
    }

    /**
     * Retrieve a list of messages which are addressed to the logged user and
     * returns a json encoded version of the list
     *
     * @return Response
     */
    public function getMessagesAction()
    {
        /** @var User $user */
        $user = $this->getUser();

        $messages = $user->getAddressedMessages();

        $results = array();

        /** @var $message Message */
        foreach ($messages as $message) {
            $results[] = $message->toArray();
        }

        $response = new Response(json_encode($results));

        return $response;
    }

    /**
     * Retrieve a list of messages which were sent by the logged user and
     * returns a json encoded version of the list
     *
     * @return Response
     */
    public function getSentMessagesAction()
    {
        /** @var User $user */
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $messages = $em->getRepository('MessageBundle:Message')->findBy(array('user' => $user->getId(), 'parent' => null));

        $results = array();

        /** @var $message Message */
        foreach ($messages as $message) {
            $results[] = $message->toArray();
        }

        $response = new Response(json_encode($results));

        return $response;
    }

    /**
     * Retrieve a message by id and all replies
     * returns a json encoded version of the message and list of replies
     *
     * @param $id
     *
     * @return Response
     */
    public function getMessageAction($id)
    {
        /** @var User $user */
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();
        $message = $em->getRepository('MessageBundle:Message')->find($id);

        if (!($message instanceof Message)) {
            return new Response(json_encode(array('error' => 'Message not found')));
        }

        if  (!($message->getUser()->getId() === $user->getId() || in_array($user, $message->getDestinationUsers()->toArray()))) {
            return new Response(json_encode(array('error' => 'Access not granted')));
        }

        $replies = $message->getReplies();

        $results = array();

        /** @var $reply Message */
        foreach ($replies as $reply) {
            $editable = ($reply->getUser()->getId() === $user->getId());
            $reply = $reply->toArray();
            $reply['editable'] = $editable;
            $results[] = $reply;
        }

        $editable = ($message->getUser()->getId() === $user->getId());

        $message = $message->toArray();
        $message['replies'] = $results;
        $message['editable'] = $editable;

        $response = new Response(json_encode($message));

        return $response;
    }

    /**
     * Get the posted data, create a new message and sent it to the given users
     *
     * @param Request $request
     *
     * @return Response
     */
    public function saveMessageAction(Request $request)
    {
        $postData = json_decode($request->getContent(), true);

        if (isset($postData['parent_id'])) {
            return $this->saveReply($postData);
        }

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $message = new Message();

//        ToDo: validation and error handling
        $message->setUser($user);
        $message->setTitle($postData['title']);
        $message->setText($postData['text']);
        $message->setCriticalDate(new \Datetime($postData['critical_date']));

        $userRepo = $em->getRepository('UserBundle:User');

        if (!is_array($postData['users'])) {
            $postData['users'] = array($postData['users']);
        }
        
        foreach ($postData['users'] as $userId) {
            $destinationUser = $userRepo->find($userId);
            if ($destinationUser instanceof User) {
                $message->addDestinationUser($destinationUser);
                $destinationUser->addAddressedMessage($message);
            }
        }

        $em->persist($message);
        $em->flush();

        $response = new Response(json_encode($message->toArray()));

        return $response;
    }

    /**
     * Update the message with given data
     *
     * @param Request $request
     * @return Response
     */
    public function editMessageAction(Request $request)
    {
        $postData = json_decode($request->getContent(), true);

        if (isset($postData['parent_id'])) {
            return $this->editReply($postData);
        }

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        /** @var Message $message */
        $message = $em->getRepository('MessageBundle:Message')->find($postData['id']);

//        ToDo: validation, security and error handling
        if (isset($postData['title'])) {
            $message->setTitle($postData['title']);
        }
        if (isset($postData['text'])) {
            $message->setText($postData['text']);
        }
        $em->flush();

        $response = new Response(json_encode($message->toArray()));

        return $response;
    }

    /**
     * Reply to a message
     *
     * @param $postData
     *
     * @return Response
     */
    public function saveReply($postData)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $message = new Message();
        $parentMessage = $em->getRepository('MessageBundle:Message')->find($postData['parent_id']);

//        ToDo: validation and error handling
        $message->setUser($user);
        $message->setParent($parentMessage);
        $message->setText($postData['text']);
        $message->setCriticalDate(new \DateTime());

        $em->persist($message);
        $em->flush();

        $response = new Response(json_encode($parentMessage->toArray()));

        return $response;
    }

    public function editReply($postData)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        /** @var Message $reply */
        $reply = $em->getRepository('MessageBundle:Message')->find($postData['id']);

//        ToDo: validation, security and error handling
        if (isset($postData['text'])) {
            $reply->setText($postData['text']);
        }

        $em->flush();

        $response = new Response(json_encode($reply->toArray()));

        return $response;
    }

    public function checkEventsAction()
    {
        //Todo: get and send notifications

        $response = new Response('test');

        return $response;
    }
}
