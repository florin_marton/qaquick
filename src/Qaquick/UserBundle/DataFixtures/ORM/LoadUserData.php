<?php
namespace Qaquick\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture,
    Doctrine\Common\DataFixtures\OrderedFixtureInterface,
    Doctrine\Common\Persistence\ObjectManager,
    Qaquick\UserBundle\Entity\User,
    Symfony\Component\DependencyInjection\ContainerInterface,
    Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $em)
    {
        $users = array(
            array(
                'username' => "user",
                'first_name' => "Test",
                'last_name' => "User",
            ),
            array(
                'username' => "lucia",
                'first_name' => "Lucia",
                'last_name' => "Negreanu",
            ),
            array(
                'username' => "daiana",
                'first_name' => "Daiana",
                'last_name' => "Harangus",
            ),
            array(
                'username' => "adrian",
                'first_name' => "Adrian",
                'last_name' => "Negreanu",
            ),
            array(
                'username' => "silviu",
                'first_name' => "Silviu",
                'last_name' => "Barta",
            ),
            array(
                'username' => "florin",
                'first_name' => "Florin",
                'last_name' => "Marton",
            ),
            array(
                'username' => "tanase",
                'first_name' => "Tanase",
                'last_name' => "Capalna",
            ),
        );

        /* @var $factory \Symfony\Component\Security\Core\Encoder\EncoderFactory */
        $factory = $this->container->get('security.encoder_factory');

        foreach($users as $entry)
        {
            $password = "secret";

            $user = new User();
            $user->setUsername($entry['username']);

            $encoder = $factory->getEncoder($user);
            $password = $encoder->encodePassword($password, $user->getSalt());
            $user->setPassword($password);

            $user->setFirstName($entry['first_name']);
            $user->setLastName($entry['last_name']);
            $user->setEmail($entry['username'] . "@lanteam-solutions.com");
            $em->persist($user);
        }

        $em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}